package com.example.data.data_sources

import com.example.core.http_clients.KtorClient.Companion.ktorClient
import com.example.domain.datasource.IUserDataSource
import com.example.domain.dtos.UserData
import io.ktor.client.call.*
import io.ktor.client.request.*

class UserDataSource : IUserDataSource {
    override suspend fun getUser(id: Int): UserData {
        return ktorClient.get("todos/1").body()
    }
}