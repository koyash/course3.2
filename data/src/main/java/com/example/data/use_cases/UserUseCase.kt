package com.example.data.use_cases

import com.example.data.data_sources.DataSources
import com.example.domain.dtos.UserData
import com.example.domain.usecases.IUserUseCase
import retrofit2.awaitResponse

class UserUseCase : IUserUseCase {

    override suspend fun getUser(id: Int): UserData? {
        if (id == 0) {
            return null
        }
        return DataSources
            .userService
            .getUserData()
            .awaitResponse()
            .body()
    }
}