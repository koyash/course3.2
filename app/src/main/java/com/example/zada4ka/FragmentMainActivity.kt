package com.example.zada4ka

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.zada4ka.compose.ComposeFragment
import com.example.zada4ka.databinding.ActivityFragmentMainBinding
import com.example.zada4ka.di_example.DiExampleFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FragmentMainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityFragmentMainBinding.inflate(layoutInflater)

        binding.firstFragmentButton.setOnClickListener {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.mainFragmentContainer, FirstFragment.newInstance())
                .addToBackStack(FirstFragment::javaClass.name)
                .commit()
        }

        setContentView(R.layout.activity_fragment_main)

        findViewById<Button>(R.id.secondFragmentButton).setOnClickListener {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.mainFragmentContainer, SecondFragment.newInstance())
                .commit()
        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainFragmentContainer, DiExampleFragment.newInstance())
            .commit()

    }
}