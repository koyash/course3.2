package com.example.zada4ka

import com.example.core.base.FragmentBase
import com.example.core.databinding.StatesBinding
import com.example.zada4ka.databinding.FragmentCleanArchBinding
import java.util.*

class CleanArchFragment : FragmentBase<FragmentCleanArchBinding, CleanArchViewModel>(
    R.id.mainFragmentContainer
) {

    override fun setUpViews() {
        super.setUpViews()
        binding.loadSmthBtn.setOnClickListener {
            viewModel.getUserData(Random().nextInt(50))
        }
    }

    override fun observeData() {
        super.observeData()
        viewModel.userDataMutable.observe(this) {
            if (it == null) binding.resultTextView.text = "Данные пусты"
            else binding.resultTextView.text = it.toString()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = CleanArchFragment()
    }

    override fun getViewModelClass(): Class<CleanArchViewModel> {
        return CleanArchViewModel::class.java
    }

    override fun getViewBinding(): FragmentCleanArchBinding {
        return FragmentCleanArchBinding.inflate(layoutInflater)
    }

    override fun getStatesBinding(): StatesBinding = binding.cleanArchFragmentStates
}