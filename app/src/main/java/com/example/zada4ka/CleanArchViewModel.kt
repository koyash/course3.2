package com.example.zada4ka

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.core.base.ViewModelBase
import com.example.core.enums.StateEnum
import com.example.data.data_sources.UserDataSource
import com.example.data.use_cases.UserUseCase
import com.example.domain.dtos.UserData
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CleanArchViewModel: ViewModelBase() {

    val userUseCase = UserUseCase()
    val userDataMutable = MutableLiveData<UserData?>()

    fun getUserData(id: Int) {
        viewModelScope.launch {
            viewState.postValue(StateEnum.Loading)
            delay(2000)
            val userDataResponse = userUseCase.getUser(id)
            userDataMutable.postValue(userDataResponse)
            viewState.postValue(StateEnum.Normal)
        }
    }

}