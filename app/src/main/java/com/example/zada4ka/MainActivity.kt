package com.example.zada4ka

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.widget.addTextChangedListener
import com.example.zada4ka.compose.ComposeFragment

class MainActivity : AppCompatActivity() {

    lateinit var myInput: EditText
    var myTextView: TextView? = null
    var tableLayout: TableLayout? = null
    var button1: Button? = null
    var button2: Button? = null


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mySp = baseContext.getSharedPreferences("MySharedPreferences", MODE_PRIVATE)
        mySp.edit()
            .putBoolean("Authorized", true)
            .putString("Token", "some_token")
            .apply()

        setContentView(R.layout.activity_main)

        myInput = findViewById(R.id.my_input)
        myTextView = findViewById(R.id.hello_text_view)
        button1 = findViewById(R.id.button1)
        tableLayout = findViewById(R.id.my_grid)

        myInput.setOnEditorActionListener { textView, i, keyEvent ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                myTextView?.setText(textView.text.toString())
            }
            true
        }

        button1?.setOnClickListener {
            Toast.makeText(this, R.string.some_secret, Toast.LENGTH_SHORT).show()
        }
        button2?.setOnClickListener {
            Toast.makeText(this, R.string.some_secret, Toast.LENGTH_SHORT).show()
        }
    }
}