package com.example.zada4ka.compose.components

import android.provider.CalendarContract
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Green
import androidx.compose.ui.graphics.Color.Companion.Red
import java.lang.reflect.Modifier

class CustomButtonComponent {
    companion object {
        @Composable
        fun CustomButtonComponent(
            onClick: () -> Unit,
            text: String,
            isDanger: Boolean = false,
        ) {
            Button(
                onClick = onClick,
                colors = if (isDanger) ButtonDefaults.buttonColors(Red) else ButtonDefaults.buttonColors(
                    Green
                )
            ) {
                Text(text)
            }
        }
    }
}