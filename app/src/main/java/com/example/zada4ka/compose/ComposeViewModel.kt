package com.example.zada4ka.compose

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.core.base.ViewModelBase
import com.example.core.enums.StateEnum
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

data class SomeDto(
    val id: Int,
    val name: String = ""
)

class ComposeViewModel: ViewModelBase() {

    val listOfData = mutableStateListOf<SomeDto>()

    init {
        loadData()
    }

    fun loadData() {
        viewModelScope.launch {
            viewState.postValue(StateEnum.Loading)
            delay(2000)
            listOfData.addAll(
                listOf(
                    SomeDto(1, "KO"),
                    SomeDto(2, "yash")
                )
            )
            viewState.postValue(StateEnum.Normal)
        }
    }

}