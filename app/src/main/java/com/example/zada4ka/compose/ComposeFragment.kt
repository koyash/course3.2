package com.example.zada4ka.compose

import android.os.Bundle
import android.os.PersistableBundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.tooling.preview.Preview
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.example.core.enums.StateEnum
import com.example.zada4ka.compose.components.CustomButtonComponent.Companion.CustomButtonComponent
import java.util.*


class ComposeFragment : Fragment() {

    companion object {
        fun newInstance() = ComposeFragment()
    }

    lateinit var viewModel: ComposeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ComposeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            viewModel.viewState.observe(viewLifecycleOwner) {
                setContent {
                    when (it) {
                        StateEnum.Loading -> CircularProgressIndicator()
                        StateEnum.Normal -> ViewWithList()
                        else -> {}
                    }
                }
            }
        }
    }

    @Composable
    fun ViewWithList() {
        LazyColumn {
            items(viewModel.listOfData) { item ->
                Card {
                    Text(item.id.toString())
                    Text(item.name)
                }
            }
        }
    }

    @Preview
    @Composable
    fun View() {
        var someText by remember { mutableStateOf("SomeText") }
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
        ) {
            CustomButtonComponent(
                isDanger = true,
                onClick = {
                    Toast.makeText(
                        requireContext(),
                        "Clicked",
                        Toast.LENGTH_SHORT
                    ).show()
                    someText = Random().nextInt().toString()
                },
                text = "Ета кнопка",
            )
            Text(someText)
        }
    }

}