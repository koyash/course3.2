package com.example.zada4ka.di_example

import android.os.Build
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import com.example.zada4ka.R
import com.example.zada4ka.databinding.FragmentDiExampleBinding
import dagger.hilt.EntryPoint
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DiExampleFragment : Fragment() {

    companion object {
        fun newInstance() = DiExampleFragment()
    }

    private lateinit var viewModel: DiExampleViewModel
    private lateinit var binding: FragmentDiExampleBinding

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDiExampleBinding.inflate(layoutInflater)

        binding.getTimeButton.setOnClickListener {
            viewModel.getTimeAtLondon()
        }

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DiExampleViewModel::class.java)
        viewModel.timeAtLondon.observe(this) {
            binding.timeResultTextView.text = it
        }
    }

}