package com.example.zada4ka.di_example.di_components

interface ITimeFetcher {
    suspend fun getTimeAtLondon(): String
}