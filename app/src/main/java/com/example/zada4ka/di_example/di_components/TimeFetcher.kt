package com.example.zada4ka.di_example.di_components

import android.os.Build
import androidx.annotation.RequiresApi
import kotlinx.coroutines.delay
import java.time.LocalDate
import javax.inject.Inject

class TimeFetcher @Inject constructor() : ITimeFetcher {

    @RequiresApi(Build.VERSION_CODES.O)
    override suspend fun getTimeAtLondon(): String {

        delay(2000)
        return LocalDate.now().toString()

    }

}