package com.example.zada4ka.di_example

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zada4ka.di_example.di_components.TimeFetcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DiExampleViewModel @Inject constructor() : ViewModel() {

    val timeAtLondon = MutableLiveData<String>()

    @Inject
    lateinit var timeFetcher: TimeFetcher

    @RequiresApi(Build.VERSION_CODES.O)
    fun getTimeAtLondon() {
        viewModelScope.launch {
            timeAtLondon.postValue(timeFetcher.getTimeAtLondon())
        }
    }

}