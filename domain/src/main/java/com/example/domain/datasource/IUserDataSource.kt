package com.example.domain.datasource

import com.example.domain.dtos.UserData

interface IUserDataSource {

    suspend fun getUser(id: Int): UserData

}