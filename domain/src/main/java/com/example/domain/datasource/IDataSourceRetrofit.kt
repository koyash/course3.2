package com.example.domain.datasource

import com.example.domain.dtos.UserData
import retrofit2.Call
import retrofit2.http.GET

interface IDataSourceRetrofit {

    @GET("todos/1")
    fun getUserData(): Call<UserData>
}