package com.example.domain.usecases

import com.example.domain.dtos.UserData

interface IUserUseCase {
    suspend fun getUser(id: Int): UserData?
}