package com.example.domain.dtos

import kotlinx.serialization.Serializable

@Serializable
data class UserData(
    val id: Int = 0,
    val title: String = ""
)
