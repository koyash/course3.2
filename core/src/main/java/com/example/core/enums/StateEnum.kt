package com.example.core.enums

enum class StateEnum {
    Loading,
    Normal,
    Error
}