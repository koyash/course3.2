package com.example.core.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.core.enums.StateEnum

open class ViewModelBase : ViewModel() {

    val viewState = MutableLiveData<StateEnum>()

}